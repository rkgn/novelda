import { readonly, writable } from "svelte/store"
import vecs_tmp from "./vecs1.json"
import topics_tmp from "./topics1.json"
import meta_data_tmp from "./meta_data.json"
import cws_tmp from "./characteristic_word.json"
import reviews_exce_tmp from "./reviews_excerpted.json"

interface Vecs {
	[key: string]: number[]
}

interface MetaData {
	[key: string]: { [key: string]: string | number }
}

export interface BookSurvey {
	"title": string,
	"similarity": number,
	"interest_level": number,
	"is_alr_read": boolean
	"is_topic_visible": boolean
}

interface ReviewsEXCE {
	[key: string]: { [key: string]: Array<string>}
}

const vecs_w = writable<Vecs>(vecs_tmp)
const topics_w = writable<string[]>(topics_tmp.topic_names)
const meta_data_w = writable<MetaData>(meta_data_tmp)
const cws_w = writable<Array<Array<string>>>(cws_tmp)
const reviews_exce_w = writable<ReviewsEXCE>(reviews_exce_tmp)
export const vecs = readonly(vecs_w)
export const topics = readonly(topics_w)
export const meta_data = readonly(meta_data_w)
export const cws = readonly(cws_w)
export const reviews_exce = readonly(reviews_exce_w)
export const is_admin = writable<boolean>(false)
export const is_topic_visible = writable<boolean>(true)
export const survey = writable<any>({})